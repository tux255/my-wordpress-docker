# How to use

1. Clone or download project to your computer
2. Edit .env file at root directory of downloaded/unzipped folder (my-wordpress-docker maybe):
    - Set Database name (optional, default: domain.com)
    - Set Database password (optional, default: aqwe123)
    - Set Database prefix (optional)
    - Default IP is 127.0.0.1 (can not change for now)

# How to import SQL file on load
1. Get your dump.sql file
2. Put it to db-dump folder inside my-wordpress-docker and name it dump.sql
3. Edit your hosts file. Example: ```127.0.0.1 domain.com www.domain.com```
4. Run ```docker-compose up``` or ```docker-compose up -d``` ( ```-d``` - silent run, demonized mode )

## P.S.
### To avoid permission errors (and the use of sudo), add your user to the docker group. Read more.
To create the docker group and add your user:
1. Create the docker group: "$ sudo groupadd docker"
2. Add your user to the docker group: "$ sudo usermod -aG docker $USER"